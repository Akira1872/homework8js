1. Опишіть своїми словами що таке Document Object Model (DOM)

DOM представляє собою весь вміст HTML сторінки у вигляді об'єктів, які можна змінювати

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

innerHTML oтримуємо вміст об'єкту разом з тегами HTML, innerText - отримуємо вміст об'єкту у вигляді тексту без тегів HTML

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

існує декілька способів звернення: 
- querySelectorAll - повертає всі елементи всередині об'єкту
- querySelector - повертає тільки перший елемент 
- getElementByID - шукає атрибут за ID
- getElementByTagName - за ім'я тегу 
- getElementByClassName - за ім'ям класу 
- getElementByName - за заданим атрибутом 

кожен спосіб відповідає за конкретну дію, яка є кращою за інші, в залежності від поставленої задачі, тому обрати кращий не є можливим 